const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [SECTION] MongoDB connection
// Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, userUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.pec4mzj.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console,"connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// [SECTION] Mongoose Schemas
// It determines the structure of our documents to be stored in the database.
// It acts as our data blueprint and guide

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Taks name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

//============================================================================
// ACTIVITY1
// 1. Create a User schema.
	// users schema
const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	}
});

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// [SECTION] Models - checks if user input is aligned with the schema
const Task = mongoose.model("Task", taskSchema);

//=============================================================================
// 2. Create a User Model.
const User = mongoose.model("User", userSchema);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// [SECTION] Creation of to do list routes

app.use(express.json());
// allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

// Creating a new task

// Business Logic
/*
1. Add a function that checks if there are duplicate tasks
	- if the task already exists in the  database, we return an error
	- if the task doesn;t exist in the database, we add it in the database.
2. The task data will be coming from teh request's body
3. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found!");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created.");
				}
			})
		}
	})
})

// Get Method
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

//==========================================================================
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Username already taken.");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, saveUser) =>{
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(202).send("Username registered.");
				}
			})
		}
	})
})

// Get Method
app.get("/signup", (req, res) => {
	User.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(203).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}.`))
